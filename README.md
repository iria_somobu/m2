# Self-hosted maven repo

## Usage

You can add repo this way:
```
repositories {
  maven { url  "https://gitlab.com/somobu/m2/-/raw/master/" }
}
```

Artifacts are stored in `somobu` group:
```
dependencies {
  implementation 'somobu:ARTIFACT:VERSION'
}
```

## Artifacts

| Name                | Local source      | Upstream source       |
| ----                | ----              | ----                  |
| discord             | [GitLab][discord] | N/A                   |
| ethemnanki          | [GitLab][ethmnk]  | N/A                   |
| image-loader        | [GitLab][uil-gl]  | [GitHub][uil-gh]      |
| opentasks-contract  | [GitLab][otc-gl]  | [GitHub][otc-gh]      |
| pircbot             | N/A               | [GitHub][pircbot-gh]  |
| sutmodel            | [GitLab][sutmod]  | N/A                   |
| utils               | [GitLab][utils]   | N/A                   |
| xsalsa20poly1305    | [GitLab][xsp-gl]  | [GitHub][xsp-gh]      |


[discord]: https://gitlab.com/iria_somobu/discord
[ethmnk]: https://gitlab.com/iria_somobu/ethemnanki
[uil-gl]: https://gitlab.com/iria_somobu/android-image-loader
[uil-gh]: https://github.com/nostra13/Android-Universal-Image-Loader
[otc-gl]: https://gitlab.com/iria_somobu/opentasks-contract
[otc-gh]: https://github.com/dmfs/opentasks
[pircbot-gh]: https://github.com/davidlazar/PircBot
[sutmod]: https://gitlab.com/somobu/bonch-data
[utils]: https://gitlab.com/iria_somobu/utils
[xsp-gl]: https://gitlab.com/iria_somobu/xsalsa20poly1305
[xsp-gh]: https://github.com/codahale/xsalsa20poly1305

